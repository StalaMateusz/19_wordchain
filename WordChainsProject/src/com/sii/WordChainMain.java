/**
 * 
 */
package com.sii;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

/**
 * The algorithm used to solve the puzzle is BFS - Breadth-First Search, traversing the 
 * tree exploring the neighbor nodes first before moving to the next level. In case of long
 * words DFS - Depth-first search, traversing as far as possible before moving to neighbors, 
 * might give better results. BFS steps:
 * - validate the words to see if the words provided are acceptable -step takes place before
 * loading the dictionary to save resources
 * - load the dictionary from the file - HashSet used so that there are no repetitive values
 * - check if the words provided exist in dictionary - due to saving resource this step
 * cannot be implemented with the first one, as dictionary does not exist yet
 * - BFS and printing out the answer in the console
 * In case of failure the corresponding exception is thrown.
 * For simplicity Design Patterns, finals, statics, setters/getters were used cautiously not to
 * create confusion and complexity in such a small program.
 * Javadoc was omitted as code is simple enough to be understood without additional instructions.
 * 
 * The key point in an algorithm is in a dictionary. The algorithm acts differently depending
 * on a dictionary. The focus was put on the most obvious cases. The more complicated
 * ones are:
 * - differentiating between words starting with an upper case. The problem at hand is that
 * the provided words have to be first changed so that every letter apart from the first one
 * is changed into lower case. After that it is required to remove all words starting with
 * upper case or lower case letter from the dictionary (depending on the words provided initially)
 * so that they would not be taken into consideration when traversing the tree. For simplicity 
 * all words have been changed to lower case.
 * - test cases where the result should be the same when reversing the order of the provided
 * words have been removed, as it is highly dependent on the dictionary itself - in this case
 * different results were given for values "dog, cat" and "cat, dog".
 * - printing more than one path - to save resources only the first correct path is saved 
 * and printed
 * - defining a language and removing the words consisting of letters from different language
 * (e.g. accepting only latin letters)
 * 
 * @author mstala
 *
 */
public final class WordChainMain {

	public Set<String> dictionary = new HashSet<>();
	public Queue<Node> queue = new LinkedList<>();
	public Node resultNode = new Node();
	public String startingPoint;
	public String endingPoint;
	public boolean matchFound = false;
	public String errorMsg;
	
	public String path = "dictionary.txt";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println("Provide starting word: ");
		String startingWord = reader.nextLine().toLowerCase();
		System.out.println("Provide ending word: ");
		String endingWord = reader.nextLine().toLowerCase();
		reader.close();
		
		try{
			wordChain(startingWord, endingWord);
		}
		catch (IllegalArgumentException | NullPointerException ex){
			ex.printStackTrace();
		}
	}
	
	public static final List<String> wordChain(String startPoint, String endPoint) throws IllegalArgumentException, NullPointerException{
		WordChainMain wcm = new WordChainMain();
		wcm.endingPoint = startPoint.toLowerCase();
		wcm.startingPoint = endPoint.toLowerCase();
		wcm.validateWords();
		wcm.loadDictionary();
		wcm.checkIfWordsAreInDictionary();
		long startTime = System.currentTimeMillis();
		wcm.BFS();
		long endTime = System.currentTimeMillis();
		double BFStime = endTime - startTime;
		
		System.out.println(wcm.resultNode.path);
		
		System.out.println("Time taken for BFS in miliseconds: "+ BFStime);
		
		return wcm.resultNode.path;
	}
	
	public final void validateWords() {
		if(this.startingPoint == null || this.endingPoint == null){
			throw new NullPointerException("Words cannot be null");
		}
		else if(this.startingPoint.isEmpty() || this.endingPoint.isEmpty()) {
			throw new IllegalArgumentException("Both words have to be provided");
		}	
		else if(!(doesContainOnlyLetters(this.startingPoint)) || !(doesContainOnlyLetters(this.endingPoint))) {
			throw new IllegalArgumentException("Both words have to contain only letters");
		}
		else if(this.startingPoint.length() != this.endingPoint.length()) {
			throw new IllegalArgumentException("Both words have to be of the same length");
		}	
	}
	
	public final void loadDictionary() {
		try(BufferedReader reader = new BufferedReader(new FileReader(this.path))) {
			String line;
			int length = this.startingPoint.length();
			while((line = reader.readLine()) != null) {
				if(line.length() == length && this.doesContainOnlyLetters(line)){
					this.dictionary.add(line.toLowerCase());
				}
			}
		}
		catch (FileNotFoundException e) {
			 e.printStackTrace();
		} 
		catch (IOException e) {
			 e.printStackTrace();
		}
	}
	
	public final void checkIfWordsAreInDictionary() {
		if(!(this.dictionary.contains(this.startingPoint)) || !(this.dictionary.contains(this.endingPoint))) {
			throw new IllegalArgumentException("Both words have to exist in a dictionary");
		}
	}
	
	public boolean doesContainOnlyLetters(String word) {
	    char[] chars = word.toCharArray();
	    for (char c : chars) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public final void BFS() {
		if(this.startingPoint.equals(this.endingPoint)) {
			return;
		}
		
		Node rootNode = new Node(this.startingPoint);
		this.queue.offer(rootNode);

		while(!this.queue.isEmpty()) {
	    	Node currentHeadNode = this.queue.poll();
	    	findAdjacentWords(currentHeadNode);
    	}
		
		if(!this.matchFound){
			throw new UnsupportedOperationException("Path does not exist");
		}
	}
	
	public void findAdjacentWords(Node headNode) {
		char[] headNodeCharArr = headNode.nodeName.toCharArray();
		
		for (int letterPosition = 0; letterPosition < headNodeCharArr.length; letterPosition++) {	     		
			char tempChar = headNodeCharArr[letterPosition];
			
			for(char newLetter = 'a'; newLetter <='z'; newLetter++) {
                 if(tempChar != newLetter) {
                	 headNodeCharArr[letterPosition] = newLetter;
                 }

                 String newWord = new String(headNodeCharArr);
                 if(this.dictionary.contains(newWord)) {
                	 Node newNode = new Node(newWord, headNode);
                	 if(newNode.nodeName.equals(this.endingPoint)) {
                		 newNode.path.add(newNode.nodeName);
                		 this.resultNode = newNode;
                		 this.matchFound = true;
                		 this.queue.clear();
                		 return;
                	 }
                	 else{
                         this.queue.offer(newNode);
                	 }
                 }
             }
			 headNodeCharArr[letterPosition] = tempChar;
		}	
	}
}
