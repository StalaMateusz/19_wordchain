package com.sii;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author mstala
 *
 */
class Node {
    
	public String nodeName;
	public final List<String> path = new ArrayList<>();

	public Node(String nodeName) {
        this.nodeName = nodeName;
    }

    public Node() {
	//  empty   
    }
   
    public Node(String nodeName, Node nodeParent) {
        this.nodeName = nodeName;
        path.addAll(nodeParent.path);
        path.add(nodeParent.nodeName);
    }
    
    @Override
    public int hashCode() {
	    return Objects.hash(path.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
        	return true;
        }
        if (obj == null) {
            return false;
        } 
        if (getClass() != obj.getClass()) {
            return false;
        }
        Node other = (Node) obj;
        return Objects.equals(path, other.path);
    }
   
    public String toString() {
        return "\n" + nodeName + ", " + path + "";
    }
}
