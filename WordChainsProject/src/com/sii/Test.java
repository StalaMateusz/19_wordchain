package com.sii;

import java.util.ArrayList;

public class Test {
	static ArrayList<String> Dict = new ArrayList<>();
	static String start = "hit";
	static String end = "cog";
	static ArrayList<ArrayList> ans = new ArrayList();
	
	public static void main(String []args){
		ArrayList ch = new ArrayList();
		Dict.add("hot");
		Dict.add("dot");
		Dict.add("dog");
		Dict.add("lot");
		Dict.add("log");
		findS(start, end, ch);
		System.out.println(ans);
	}

	public static void findS(String s, String e, ArrayList ch){
		char[] sToC = s.toCharArray();
		ch.add(s);
		
		for(int i=0;i<sToC.length;i++){
			char old=sToC[i];
			for(char c='a'; c<'z';c++){
				sToC[i]=c;
				
				if((String.valueOf(sToC)).equals(end)){
					ch.add(String.valueOf(sToC));
					System.out.println("Done");
					ans.add(ch);
					return;
				}
				
				if(Dict.contains(String.valueOf(sToC))){
					if(!ch.contains(String.valueOf(sToC))){
						ArrayList newA = copy(ch);
						System.out.println("Gonna call recurse with " + String.valueOf(sToC));
						findS(String.valueOf(sToC),end,newA);
					}
				}
			}
			sToC[i]=old;
		}
	}

	public static ArrayList copy(ArrayList ch){
		ArrayList newA = new ArrayList();
		for(int i=0;i<ch.size();i++){
			newA.add(ch.get(i));
		}
		return newA;
	}
}
