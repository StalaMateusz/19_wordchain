package com.sii;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

public class WordChainTest {
	
	@Test
	public void testCheckIfProvidingTheSameWordReturnsZero() {
		assertEquals(Arrays.asList(), WordChainMain.wordChain("cat", "cat"));
	}
	
	@Test
	public void testCheckIfTheFunctionIsIdempotent() {
		List<String> result1 = WordChainMain.wordChain("cat", "dog");
		List<String> result2 = WordChainMain.wordChain("cat", "dog");
		int i = 0;
		while(i < 5){
			result2 = WordChainMain.wordChain("cat", "dog");
			i++;
		}
		assertEquals(result1, result2);		
	}

	@Test
	public void testCheckIfTheWordsCaseDoesNotChangeTheResult() {
		List<String> result1 = WordChainMain.wordChain("caT", "DOg");
		List<String> result2 = WordChainMain.wordChain("cat", "dog");
		assertEquals(result1, result2);		
	}
	
//	@Test
//	public void testIfReverseOrderAffectsResult() {
//		List<String> result1 = WordChainMain.wordChain("cat", "dog");
//		List<String> result2 = WordChainMain.wordChain("dog", "cat");
//		Collections.reverse(result2);
//		assertEquals(result1, result2);		
//	}
}
