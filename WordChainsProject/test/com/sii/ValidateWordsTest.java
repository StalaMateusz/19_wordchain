package com.sii;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

public class ValidateWordsTest {

	public static Set<String> dictionary;
	public static WordChainMain validationTest;
	public String word1 = "";
	public String word2 = "";
	public int i;
	
	@BeforeClass
	public static void initialize(){
		dictionary = new HashSet<>();
		dictionary.add("cat");
		dictionary.add("cap");
		dictionary.add("sat");
		dictionary.add("rap");
		dictionary.add("cop");
		dictionary.add("cab");
		dictionary.add("rat");
		dictionary.add("car");
		dictionary.add("sap");
		dictionary.add("pat");
		dictionary.add("pot");
		dictionary.add("bic");
		
		validationTest = new WordChainMain();
		validationTest.dictionary = dictionary;
	}
	
	@Test(expected = NullPointerException.class)
	public void testCheckIfProvidingNullFails() {
		validationTest.startingPoint = null;
		validationTest.endingPoint = null;
		validationTest.validateWords();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCheckIfProvidingEmptyStringFails() {
		validationTest.startingPoint = "cat";
		validationTest.endingPoint = "";
		validationTest.validateWords();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCheckIfProvidingWordsNotExistingInDictionaryFails() {
		validationTest.startingPoint = "cat";
		validationTest.endingPoint = "ccc";
		validationTest.checkIfWordsAreInDictionary();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCheckIfProvidingStringsOfDifferentLengthFails() {
		validationTest.startingPoint = "cat";
		validationTest.endingPoint = "cccc";
		validationTest.validateWords();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCheckIfProvidedStringsOnlyConsistsOfCharacters() {
		validationTest.startingPoint = "c't";
		validationTest.endingPoint = "cc3";
		validationTest.validateWords();
	}
}
